import { makeAutoObservable } from "mobx";
import { IIncomingDataElem, ITodo } from "../interfaces/interfaces";
import { IncomingDataArr, TodosArr } from "../types/types";

class ToDoList {
    toDoList: TodosArr = [];

    constructor() {
        makeAutoObservable(this);
    }

    addNewTodo(todo: ITodo) {
        this.toDoList.push(todo);
    }

    updateStoreOnload(todos: TodosArr) {
        if (!todos) {
            return;
        }
        
        this.toDoList.push(...todos);
    }

    removeTodo(id: number) {
        this.toDoList = this.toDoList.filter((item: ITodo) => item.id !== id)
    }

    setDone(id: number) {
        const elem: ITodo = this.toDoList.find((item: ITodo) => item.id === id)!;
        elem.finished = !elem.finished;
    }

    clearStore() {
        this.toDoList.length = 0;
    }

    getExternalData() {
        const filterElem = (data: IncomingDataArr) => {
            data.forEach((item: IIncomingDataElem) => {
                const newTodo: ITodo = {id: Math.random() * 100, value: item.title, finished: false, existDate: new Date()};
                this.toDoList.push(newTodo);
            });
        };

        fetch('https://jsonplaceholder.typicode.com/posts')
            .then((response) => response.json())
            .then((json) => filterElem(json));
    }
}

export default new ToDoList();