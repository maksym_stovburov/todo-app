import { Dispatch, SetStateAction } from "react";

interface ITodo {
    id: number;
    value: string;
    finished: boolean;
    existDate: Date;
}

interface IDataElem {
    name: string;
    Closed_tasks: number;
}

interface IIncomingDataElem {
    userId: number;
	id: number;
	title: string;
	completed: boolean;
}

interface INewTodoProps {
    setActive: Dispatch<SetStateAction<boolean>>;
}

export type { ITodo, IDataElem, IIncomingDataElem, INewTodoProps };