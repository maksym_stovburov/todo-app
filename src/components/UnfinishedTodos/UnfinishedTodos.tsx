import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import todoList from '../../store/todoList';
import { Timer, TodosArr } from '../../types/types';
import { ITodo } from '../../interfaces/interfaces';
import './UnfinishedTodos.scss';

const UnfinishedTodos: React.FC = observer(() => {
    const unfinishedTodos: TodosArr = todoList.toDoList.filter((item: ITodo) => item.finished !== true);
    let timer: Timer;

    useEffect(() => {
        return(() => {
            clearTimeout(timer);
        })
    })

    const handleItemRemove = (id: number) => {
        todoList.removeTodo(id);
    }

    const handleItemFinished = (id: number) => {
        timer = setTimeout(() => {
            todoList.setDone(id);
        }, 200);
    }

    return (
        <div className="main__unfinishedTodos todosGeneral">
            {unfinishedTodos.length ? 
            <ul>
                {unfinishedTodos.map((item: ITodo) => (
                    <li key={item.id + Math.random() * 100}>
                        <button type="button" className="deleteBtn" onClick={() => handleItemRemove(item.id)}>delete</button>
                        <span>
                            {item.value}
                        </span>
                        <input type="checkbox" name="checkdone" id="checkdone" title="Mark as done" onClick={() => handleItemFinished(item.id)}/>
                    </li>
                ))}
            </ul> :
            <div className="todosGeneral__empty">
                <h3>There is no open tasks yet</h3>
            </div>
            }
        </div>
    );
});

export default UnfinishedTodos;
