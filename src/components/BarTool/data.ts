import { DataArr } from '../../types/types'

const data: DataArr = [
    {
        name: "Sunday",
        Closed_tasks: 0,
    },
    {
        name: "Monday",
        Closed_tasks: 0,
    },
    {
        name: "Tuesday",
        Closed_tasks: 0,
    },
    {
        name: "Wednesday",
        Closed_tasks: 0,
    },
    {
        name: "Thursday",
        Closed_tasks: 0,
    },
    {
        name: "Friday",
        Closed_tasks: 0,
    },
    {
        name: "Saturday",
        Closed_tasks: 0,
    }
    ];

export { data };