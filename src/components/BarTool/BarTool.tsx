import React, { useState } from "react";
import {
BarChart,
Bar,
XAxis,
YAxis,
CartesianGrid,
Tooltip,
Legend
} from "recharts";
import todoList from "../../store/todoList";
import { data } from "./data";
import { TodosArr } from "../../types/types";
import { IDataElem, ITodo } from "../../interfaces/interfaces";
import "./BarTool.scss"

const BarTool: React.FC = () => {
    const [ready, setReady] = useState(false);

    const getTasks = () => {
        const finishedTodos: TodosArr = todoList.toDoList.filter((item: ITodo) => item.finished === true);

        data.forEach((item: IDataElem) => item.Closed_tasks = 0);
        finishedTodos.forEach((item: ITodo) => {
            const day: number = new Date(item.existDate).getDay();
            data[day].Closed_tasks++;
        });
        setReady(true);
    };

    return (
        <div className="barTool">
            {ready ?
                <BarChart
                width={600}
                height={300}
                data={data}
                margin={{
                    top: 5,
                    right: 5,
                    left: 5,
                    bottom: 5
                }}
                >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="Closed_tasks" fill="#508ac4" />
                </BarChart> :
                <button type="button" className="barTool__getBtn" onClick={getTasks}>get graphs</button>
            }
            {ready ? 
                <button type="button" className="barTool__closeBtn" onClick={() => setReady(false)}>close</button> :
                <></> 
            }
        </div>
    );
}

export default BarTool;
