import React, { useEffect, useState } from 'react';
import { Link, Outlet } from "react-router-dom";
import { observer } from 'mobx-react-lite';
import todoList from '../../store/todoList';
import AddNewTodo from '../AddNewTodo/AddNewTodo';
import { StateFinished } from '../../types/types';
import './Todos.scss';

const Todos: React.FC = observer(() => {
    const [active, setActive] = useState(false);
    const [finished, setFinished] = useState<StateFinished>(null);

    useEffect(() => {
        window.addEventListener("beforeunload", () => {
            localStorage.setItem('todo-data', JSON.stringify(todoList.toDoList));
        });

        todoList.updateStoreOnload(JSON.parse(localStorage.getItem('todo-data')!));
    }, [])

    return (
        <main className="main">
            <h2>Simple tasklist editor</h2>
            <div className="main__container">
                <div className="main__container__addTodoContainer addTodoContainer">
                    {active ?
                    <AddNewTodo setActive={setActive} /> :
                    <button type="button" className="addTodoContainer__addBtn" onClick={() => setActive(true)}>add new todo</button>
                    }
                </div>
                <div className="main__container__todosContainer todosContainer">
                    <div className="todosContainer__btnsContainer">
                        <Link to="/open" onClick={() => setFinished(false)}>open<div className={!finished && finished !== null ? "" : "hidden"}/></Link>
                        <Link to="/closed" onClick={() => setFinished(true)}>closed<div className={finished && finished !== null ? "" : "hidden"}/></Link>
                        <button type="button" className="todosContainer__btnsContainer-getDataBtn" onClick={() => todoList.getExternalData()}>get external data</button>
                    </div>
                    <button type="button" className="todosContainer__clearBtn" onClick={() => todoList.clearStore()}>clear all</button>
                    <Outlet />
                </div>
            </div>
        </main>
    );
});

export default Todos;
